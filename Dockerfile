FROM ubuntu

WORKDIR /home

COPY . .

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install maven -y

RUN mvn compile
RUN mvn test