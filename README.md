Hello dear visitor! 

This repo contains sample tests for my Medium article [Rest-Assured project integration with Gitlab CI and Allure TestOps](https://medium.com/@artemmezdrin/25584f13f5a1?source=friends_link&sk=0eaa0edbc0533edfcd2112e3a6757781).

[Main](https://gitlab.com/Amezdrin/medium-tests/-/tree/main) branch contains code and gitlab-ci config only for basic guide.

[Allure-testops](https://gitlab.com/Amezdrin/medium-tests/-/tree/allure-tesops) branch contains code and gitlab-ci config for TestOps integration.

Feel free to use it as a template, but please don't forget to open the article and click 'Applaud' :)